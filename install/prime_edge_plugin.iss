#define MyAppName "Prime Edge Browser Plugin"
#define MyAppVersion "1.1.0"
#define MyAppPublisher "Chyron"
#define MyAppURL "http://www.chyron.com"
#define PluginsFolder "C:\Chyron\Prime\Plugins"

[Setup]
; NOTE: The value of AppId uniquely identifies this application. Do not use the same AppId value in installers for other applications.
; (To generate a new GUID, click Tools | Generate GUID inside the IDE.)
AppId={{92DC6A94-57E3-4302-8D34-25D082383B32}
AppName={#MyAppName}
AppVersion={#MyAppVersion}
AppPublisher={#MyAppPublisher}
AppPublisherURL={#MyAppURL}
AppSupportURL={#MyAppURL}
AppUpdatesURL={#MyAppURL}
AppendDefaultDirName=no
DefaultDirName={#PluginsFolder}
DefaultGroupName={#MyAppName}
DisableProgramGroupPage=yes
OutputBaseFilename=Prime Edge Browser Plugin {#MyAppVersion}
Compression=lzma
SolidCompression=yes
WizardStyle=modern
Uninstallable = no

[Languages]
Name: "english"; MessagesFile: "compiler:Default.isl"

[Files]
Source: "..\Plugin\ChyronHego.Prime.Plugins.UrlResource.dll"; DestDir: "{app}"; Flags: ignoreversion
; NOTE: Don't use "Flags: ignoreversion" on any shared system files

