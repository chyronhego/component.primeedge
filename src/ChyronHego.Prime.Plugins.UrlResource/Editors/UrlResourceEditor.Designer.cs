﻿namespace ChyronHego.Prime.Plugins.UrlResource.Editors
{
    partial class UrlResourceEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UrlResourceEditor));
      this.panel = new System.Windows.Forms.Panel();
      this.openBehaviorComboBox = new System.Windows.Forms.ComboBox();
      this.openBehaviorLabel = new System.Windows.Forms.Label();
      this.urlLabel = new System.Windows.Forms.Label();
      this.urlTextBox = new System.Windows.Forms.TextBox();
      this.navigateButton = new System.Windows.Forms.Button();
      this.panel.SuspendLayout();
      this.SuspendLayout();
      // 
      // panel
      // 
      this.panel.Controls.Add(this.openBehaviorComboBox);
      this.panel.Controls.Add(this.openBehaviorLabel);
      this.panel.Controls.Add(this.urlLabel);
      this.panel.Controls.Add(this.urlTextBox);
      this.panel.Controls.Add(this.navigateButton);
      resources.ApplyResources(this.panel, "panel");
      this.panel.Name = "panel";
      // 
      // openBehaviorComboBox
      // 
      this.openBehaviorComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.openBehaviorComboBox.FormattingEnabled = true;
      resources.ApplyResources(this.openBehaviorComboBox, "openBehaviorComboBox");
      this.openBehaviorComboBox.Name = "openBehaviorComboBox";
      // 
      // openBehaviorLabel
      // 
      resources.ApplyResources(this.openBehaviorLabel, "openBehaviorLabel");
      this.openBehaviorLabel.Name = "openBehaviorLabel";
      // 
      // urlLabel
      // 
      resources.ApplyResources(this.urlLabel, "urlLabel");
      this.urlLabel.Name = "urlLabel";
      // 
      // urlTextBox
      // 
      resources.ApplyResources(this.urlTextBox, "urlTextBox");
      this.urlTextBox.Name = "urlTextBox";
      // 
      // navigateButton
      // 
      resources.ApplyResources(this.navigateButton, "navigateButton");
      this.navigateButton.Name = "navigateButton";
      this.navigateButton.Text = global::ChyronHego.Prime.Plugins.UrlResource.Properties.Resources.NavigateText;
      // 
      // UrlResourceEditor
      // 
      resources.ApplyResources(this, "$this");
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.panel);
      this.Name = "UrlResourceEditor";
      this.panel.ResumeLayout(false);
      this.panel.PerformLayout();
      this.ResumeLayout(false);

        }

    #endregion

    private System.Windows.Forms.Panel panel;
    private System.Windows.Forms.ComboBox openBehaviorComboBox;
    private System.Windows.Forms.Label openBehaviorLabel;
    private System.Windows.Forms.Label urlLabel;
    private System.Windows.Forms.TextBox urlTextBox;
    private System.Windows.Forms.Button navigateButton;
  }
}