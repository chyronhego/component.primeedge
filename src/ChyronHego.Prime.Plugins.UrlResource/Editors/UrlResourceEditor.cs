﻿namespace ChyronHego.Prime.Plugins.UrlResource.Editors
{
    using System;
    using System.Diagnostics;
    using ChyronHego.Prime.Plugin.Editor;
    using ChyronHego.Prime.Plugin.Objects;
    using ChyronHego.Prime.Plugins.UrlResource.Objects;
    using ChyronHego.Prime.Scene;

    public partial class UrlResourceEditor : PluginEditorBase
    {
        private UrlResourceObject urlResource;

        public UrlResourceEditor()
        {
            InitializeComponent();

            openBehaviorComboBox.DataSource = Enum.GetValues(typeof(UrlOpenBehavior));
        }

        public override IPluginObject PluginObject
        {
            set
            {
                if (urlResource != null)
                {
                    urlResource.PropertyChanged -= UrlResource_PropertyChanged;
                }

                if (value is UrlResourceObject urlResourceObject)
                {
                    urlTextBox.TextChanged -= UrlTextBox_TextChanged;
                    openBehaviorComboBox.SelectedIndexChanged -= OpenBehaviorComboBox_SelectedIndexChanged;

                    urlResource = urlResourceObject;

                    openBehaviorComboBox.SelectedItem = urlResource.OpenBehavior;

                    UpdateTextFromObject();

                    urlResource.PropertyChanged += UrlResource_PropertyChanged;
                    urlTextBox.TextChanged += UrlTextBox_TextChanged;
                    openBehaviorComboBox.SelectedIndexChanged += OpenBehaviorComboBox_SelectedIndexChanged;
                }
            }
        }

        private void OpenBehaviorComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            var openBehavior = (UrlOpenBehavior)openBehaviorComboBox.SelectedItem;

            urlResource.OpenBehavior = openBehavior;
        }

        private void UpdateTextFromObject()
        {
            urlTextBox.Text = urlResource.Url ?? string.Empty;
        }

        private void UrlTextBox_TextChanged(object sender, EventArgs e)
        {
            urlResource.Url = urlTextBox.Text;
        }

        private void UrlResource_PropertyChanged(ISceneObject sceneObject, string property)
        {
            if (property == PropertyType.Url)
            {
                urlTextBox.TextChanged -= UrlTextBox_TextChanged;
                UpdateTextFromObject();
                urlTextBox.TextChanged += UrlTextBox_TextChanged;
            }
        }

        private void NavigateButton_Click(object sender, EventArgs e)
        {
            var url = urlResource.Url;

            if (string.IsNullOrWhiteSpace(url))
            {
                urlResource.Scene.LogWarning(urlResource, "The URL is empty.");
            }
            else
            {
                Navigate(url);
            }
        }

        private static void Navigate(string url)
        {
            if (!Uri.IsWellFormedUriString(url, UriKind.Absolute))
            {
                url = $"https://{url}";
            }

            var processStartInfo = new ProcessStartInfo(url) { UseShellExecute = true };

            new Process { StartInfo = processStartInfo }.Start();
        }
    }
}
