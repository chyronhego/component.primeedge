﻿namespace ChyronHego.Prime.Plugins.UrlResource.Objects
{
    using System.ComponentModel;
    using ChyronHego.Enterprise.Infrastructure.Resources;

    [TypeConverter(typeof(ResourceEnumConverter))]
    public enum UrlOpenBehavior
    {
        NewTabWithFocus,
        NewTab,
        NewWindowWithFocus,
        NewWindow
    }
}
