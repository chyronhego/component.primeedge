﻿namespace ChyronHego.Prime.Plugins.UrlResource.Objects
{
    using System.Collections.Generic;
    using ChyronHego.Prime.Plugin.Objects;
    using ChyronHego.Prime.Plugins.UrlResource.Properties;
    using ChyronHego.Prime.Scene;

    public class UrlResourceObject : PluginObjectBase
    {
        public UrlResourceObject()
        {
            Url = "";
        }

        public const string NavigateCommandName = "Navigate";

        protected override string IconName => "url_resource_icon";

        protected override string DefaultName => Resources.BrowserText;

        protected override IEnumerable<string> Commands => new[] { NavigateCommandName };

        public UrlOpenBehavior OpenBehavior
        {
            get => GetValue<UrlOpenBehavior>(nameof(OpenBehavior));
            set => SetValue(nameof(OpenBehavior), value);
        }

        public string Url
        {
            get => GetValue<string>(PropertyType.Url);
            set => SetValue(PropertyType.Url, value);
        }

        public void Navigate()
        {
            Execute(NavigateCommandName);
        }
    }
}