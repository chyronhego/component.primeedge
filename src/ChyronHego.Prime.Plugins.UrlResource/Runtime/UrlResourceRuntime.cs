﻿namespace ChyronHego.Prime.Plugins.UrlResource.Runtime
{
    using ChyronHego.Prime.Plugin.Runtime;
    using ChyronHego.Prime.Plugins.UrlResource.Objects;
    using ChyronHego.Prime.Scene.Objects.Interfaces;

    public class UrlResourceRuntime : PluginRuntimeBase
    {
        private ICommandObject urlResource;

        public override void Load()
        {
            if (PluginObject is UrlResourceObject urlResourceObject)
            {
                urlResource = urlResourceObject;
                urlResource.CommandExecuted += UrlResourceRuntime_CommandExecuted;
            }
        }

        public override void Play()
        {
        }

        public override void Stop()
        {
        }

        public override void Clear()
        {
            if (urlResource != null)
            {
                urlResource.CommandExecuted -= UrlResourceRuntime_CommandExecuted;
                urlResource = null;
            }
        }

        private void UrlResourceRuntime_CommandExecuted(ICommandObject parent, string command)
        {
            if (parent is UrlResourceObject urlResourceObject && command == UrlResourceObject.NavigateCommandName)
            {
                urlResourceObject.Scene.Status = $"Edge Browser Plugin navigates to: '{urlResourceObject.Url}'";
            }
        }
    }
}