﻿namespace ChyronHego.Prime.Plugins.UrlResource
{
    using System.Collections.Generic;
    using ChyronHego.Prime.Plugin;
    using ChyronHego.Prime.Plugin.Settings;
    using ChyronHego.Prime.Plugins.UrlResource.Editors;
    using ChyronHego.Prime.Plugins.UrlResource.Objects;
    using ChyronHego.Prime.Plugins.UrlResource.Properties;
    using ChyronHego.Prime.Plugins.UrlResource.Runtime;

    public class UrlResourcePlugin : IPlugin
    {
        public UrlResourcePlugin()
        {
            ObjectDefinitions = new[]
            {
                new PluginObjectDefinition
                {
                    Name = Resources.BrowserText,
                    ObjectType = typeof(UrlResourceObject),
                    EditorType = typeof(UrlResourceEditor),
                    RuntimeType = typeof(UrlResourceRuntime)
                }
            };

            // we don't need settings in Url Resource Plugin
            Settings = null;
        }

        public string Name => Resources.UrlResourcePluginName;  

        public IEnumerable<PluginObjectDefinition> ObjectDefinitions { get; }

        public IPluginSettings Settings { get; }
    }
}